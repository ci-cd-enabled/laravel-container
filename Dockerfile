FROM registry.gitlab.com/devopstwzl/apache2-php-base-image:8.2-laravel
LABEL authors="Tom Sievers <t.sievers@windesheim.nl>"
LABEL description="Hello world Laravel test project by Tom Sievers"

USER app

EXPOSE 8000
EXPOSE 5173
