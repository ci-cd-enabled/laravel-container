# Laravel Container

Optie 1:
Kopieer de bestanden `docker-compose.yml`, `.dockerignore`, `.gitlab-ci.yml` en `goss.yaml` (in de map tests) in je eigen project (zet de goss.yaml ook weer in een tests map). In de package.json moet je bij vite-dev "vite" vervangen door "vite --host" zie het voorbeeld in dit project!

Optie 2 ?(Mogelijk nog niet compleet):
Clone deze folder en de container maakt zelf een nieuw Laravel project aan.


Pipeline (Alleen nodig voor DevOps en deployment, voor development kun je de standaard image gebruiken):
Upload project in GitLab, koppel een runner en kijk of de eerste job van de pipeline succesvol afrond. Kijk dan in GitLab bij Deploy -> Container Registry Klik op het klembord icoon en plak in de docker-compose.yml dan bij image die link met :latest er achter (bijvoorbeeld https://registry.gitlab.com/ci-cd-enabled/laravel-container:latest voor dit voorbeeld project).

Uitvoeren:
Klik in PHPStorm met rechts op docker-compose.yml op `Run File`, mogelijk moet je op Windows docker-compose installeren? \@TODO Tom -> Hoe beschrijven?.

Er wordt gekeken naar het vendor/autoload.php bestand, als deze niet bestaat wordt het volgende uitgevoerd
```bash
composer install --optimize-autoloader
npm install
php artisan key:generate
php artisan migrate:fresh --seed
```
Wil je dat dus forceren, verwijder dan de vendor map.

\@TODO goss faalt nog omdat goss runt voordat het project klaar is met opzetten.

